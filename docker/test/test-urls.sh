#!/usr/bin/env bash

set -euo pipefail

default_request_base="https://staging.gitter.im"
request_base="${1:-$default_request_base}"
echo "request_base: $request_base"

test_urls=$(
  cat <<-EOF
  gitter/gitter/archives
  gitter/gitter/archives/all
  gitter/gitter/archives/2022/02/13
  饥人谷/直播教室
  javaintroonline/болталка
  gitter/gitter/archives
  ipfs/ipfs/ipfs-portland-meetup
  ipfs/ipfs/ipfs-portland-meetup/archives/all
  gitter/gitter/archives/2021/01/01
EOF
)

result_urls=$(
  cat <<EOF
302 https://app.gitter.im/#/room/#gitter_gitter:gitter.im
302 https://app.gitter.im/#/room/#gitter_gitter:gitter.im
302 https://app.gitter.im/#/room/#gitter_gitter:gitter.im
301 https://app.gitter.im/#/room/#饥人谷_直播教室:gitter.im
301 https://app.gitter.im/#/room/#javaintroonline_болталка:gitter.im
302 https://app.gitter.im/#/room/#gitter_gitter:gitter.im
301 https://app.gitter.im/#/room/#ipfs_ipfs_ipfs-portland-meetup:gitter.im
302 https://app.gitter.im/#/room/#ipfs_ipfs_ipfs-portland-meetup:gitter.im
302 https://app.gitter.im/#/room/#gitter_gitter:gitter.im
EOF
)
SAVEIFS=$IFS               # Save current IFS (Internal Field Separator)
IFS=$'\n'                  # Change IFS to newline char
result_urls=($result_urls) # split the `names` string into an array by the same name
IFS=$SAVEIFS               # Restore original IFS

echo "test_urls: $test_urls"

idx=0
for url in $test_urls; do
  full_url="$request_base/$url"
  echo "url $idx: $full_url"
  output="$(curl -w "%{http_code} %{redirect_url}" --silent --output /dev/null "$full_url")"
  echo "$output"

  target_url="${result_urls[idx]}"
  if [ "$output" == "$target_url" ]; then
    echo "Matched"
  else
    echo "!!! Unmatched !!!"
    echo "$output != $target_url"
    exit 1
  fi

  idx=$((idx + 1))
done
