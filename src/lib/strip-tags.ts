
/**
 *  Strips a string of any HTML tag.
 *  Note that `stripTags` will only strip HTML4.01 tags (such as - div, span and abbr)
 *  It will not strip namespace-prefixed tags such as "h:table" or "xsl:template"
 * 
 * via https://github.com/kangax/prototype/blob/a223833c8b49ae55f03b1e1a3a5b7e9fb647c139/src/lang/string.js#L124-L133
**/
function stripTags(inputString: string): string {
  return inputString.replace(/<\w+(\s+("[^"]*"|'[^']*'|[^>])+)?>|<\/\w+>/gi, '');
}

export default stripTags;
