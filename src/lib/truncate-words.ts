function truncateWords(inputString: string, numWords: number): string {
  return inputString.split(' ').slice(0, numWords).join(' ');
}

export default truncateWords;
