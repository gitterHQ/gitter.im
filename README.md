# Gitter static site (`gitter.im`)

The static site that drives the Gitter homepage and documentation, see <https://gitter.im/>

Using [Astro](https://astro.build/) as the static site generator.

We mostly just point to `app.gitter.im` (Gitter rebranded instance of Element) and provide people info on the Matrix nature of Gitter now.

## Setup (development)

1. `npm install`
1. `npm run dev`
1. Visit <http://localhost:3000/>

## Build (production)

1. `npm run build`
1. This will produce assets in the `dist/` directory which can be copied over to Cloudflare pages

## Dev notes

### Styleguide

We have a few components laid out in the styleguide at <http://localhost:3000/styleguide>

### `badges.gitter.im`

The assets under `/public/images/badges/` are used for `badges.gitter.im`.

See this Nginx config for how they're utilized (TODO)

# Deployment

Deployed from the [helm](./helm) path of this repo via ArgoCD - synchronized upon Webhook triggered by pushes to any branch.

To deploy a new copy, update the image tag in [`helm/values.staging.yaml`](./helm/values.staging.yaml) or [`helm/values.production.yaml`](./helm/values.production.yaml) and merge/push to `main`.

- [Staging Environment: ArgoCD](https://argocd.infra.element.dev/applications/gitter-im)
- [Production Environment: ArgoCD](https://argocd.infra.element.io/applications/gitter-im)

## Useful links for monitoring and managing the deploys

### Staging Environment

- Graphs
  - [K8s Compute resources (pods) - `gitter-im` namespace](https://grafana.infra.element.dev/d/85a562078cdf77779eaa1add43ccec1e/kubernetes-compute-resources-namespace-pods?orgId=1&refresh=10s&var-datasource=default&var-cluster=hss-staging-eu-north-1&var-namespace=gitter-im)
  - [nginx-ingress - `badges.staging.gitter.im`](https://grafana.infra.element.dev/d/nginx/nginx-ingress-controller?orgId=1&refresh=5s&var-DS_PROMETHEUS=VictoriaMetrics&var-namespace=All&var-controller_class=k8s.io%2Fpublic-ingress-nginx&var-controller=All&var-ingress=gitter-im-badges)
  - [nginx-ingress - `staging.gitter.im`](https://grafana.infra.element.dev/d/nginx/nginx-ingress-controller?orgId=1&refresh=5s&var-DS_PROMETHEUS=VictoriaMetrics&var-namespace=All&var-controller_class=k8s.io%2Fpublic-ingress-nginx&var-controller=All&var-ingress=gitter-im)
- Logs: [Kubernetes Logs - `gitter-im` namespace](https://grafana.infra.element.io/d/logs-viewer/kubernetes-logs-namespace-pod-container?orgId=1&var-datasource=Loki&var-namespace=gitter-im&var-pod=All&var-container=All&var-search=)
- ArgoCD: [`gitter-im`](https://argocd.infra.element.dev/applications/gitter-im?view=tree&resource=)

### Production Environment

- Graphs
  - [K8s Compute resources (pods) - `gitter-im` namespace](https://grafana.infra.element.io/d/85a562078cdf77779eaa1add43ccec1e/kubernetes-compute-resources-namespace-pods?orgId=1&refresh=10s&var-datasource=default&var-cluster=hss-production-eu-central-1&var-namespace=gitter-im)
  - [nginx-ingress - `badges.gitter.im`](https://grafana.infra.element.io/d/nginx/nginx-ingress-controller?orgId=1&refresh=5s&var-DS_PROMETHEUS=VictoriaMetrics&var-namespace=All&var-controller_class=k8s.io%2Fpublic-ingress-nginx&var-controller=All&var-ingress=gitter-im-badges)
  - [nginx-ingress - `gitter.im`](https://grafana.infra.element.io/d/nginx/nginx-ingress-controller?orgId=1&refresh=5s&var-DS_PROMETHEUS=VictoriaMetrics&var-namespace=All&var-controller_class=k8s.io%2Fpublic-ingress-nginx&var-controller=All&var-ingress=gitter-im)
- Logs: [Kubernetes Logs - `gitter-im` namespace](https://grafana.infra.element.io/d/logs-viewer/kubernetes-logs-namespace-pod-container?orgId=1&var-datasource=Loki&var-namespace=gitter-im&var-pod=All&var-container=All&var-search=)
- ArgoCD: [`gitter-im`](https://argocd.infra.element.io/applications/gitter-im?view=tree&resource=)
